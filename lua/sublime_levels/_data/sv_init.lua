--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

local SQL   = {};
local path  = Sublime.GetCurrentPath();

---
--- CreateTables
---
function SQL:CreateTables()
    if (not sql.TableExists("Sublime_Levels")) then
        if (sql.Query([[CREATE TABLE Sublime_Levels (
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            SteamID VARCHAR(17),
            Level INTEGER,
            Experience INTEGER,
            TotalExperience INTEGER,
            NeededExperience INTEGER,
            Name VARCHAR(32),
            Unique(SteamID)
        );]]) == false) then
            Sublime.Print("SQL Error: %s", sql.LastError());
        else
            Sublime.Print("Successfully created sql table: Sublime_Levels");
        end
    end

    if (not sql.TableExists("Sublime_Skills")) then
        if (sql.Query([[CREATE TABLE Sublime_Skills (
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            SteamID VARCHAR(17),
            Points INTEGER,
            Points_Spent INTEGER,
            Skill_Data TEXT,
            Unique(SteamID)
        );]]) == false) then
            Sublime.Print("SQL Error: %s", sql.LastError());
        else
            Sublime.Print("Successfully created sql table: Sublime_Skills");
        end
    end

    if (not sql.TableExists("Sublime_Data")) then
        if (sql.Query([[CREATE TABLE Sublime_Data (
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            ExperienceGained INTEGER,
            LevelsGained INTEGER
        );]]) == false) then
            Sublime.Print("SQL Error: %s", sql.LastError());
        else
            sql.Query("INSERT INTO Sublime_Data (ExperienceGained, LevelsGained) VALUES('0', '0')");

            Sublime.Print("Successfully created sql table: Sublime_Data");
        end
    end
end

---
--- FormatSQL
---
function SQL:FormatSQL(formatString, ...)
	local repacked 	= {};
	local args		= {...};
	
	for _, arg in ipairs(args) do 
		table.insert(repacked, sql.SQLStr(arg, true));
	end

	return string.format(formatString, unpack(repacked));
end

hook.Add("Initialize", path, function()
    SQL:CreateTables();
end);

---
--- GetSQL
---
function Sublime.GetSQL()
    return SQL;
end