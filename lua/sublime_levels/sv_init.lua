--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

util.AddNetworkString("Sublime.Notify");

---
--- Sublime.Notify
---
function Sublime.Notify(ply, msg)
    net.Start("Sublime.Notify");
        net.WriteString(msg);
    net.Send(ply);
end