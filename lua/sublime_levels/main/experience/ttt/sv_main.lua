--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

local path = Sublime.GetCurrentPath();

hook.Add("PlayerDeath", path, function(victim, _, attacker)
    if (IsValid(victim) and IsValid(attacker) and attacker:IsPlayer()) then
        if (GAMEMODE and GAMEMODE.ThisClass and GAMEMODE.ThisClass:find("terrortown")) then
            if (victim == attacker) then
                return;
            end
            
            --
            -- Refuse experience to the killer if he killed a ghost
            -- This is for Tommy's SpecDM.
            -- https://github.com/Tommy228/TTT_Spectator_Deathmatch
            --

            if (SpecDM and not Sublime.Config.SpecExperience and victim:IsGhost()) then
                return;
            end

            ---
            --- Teamkilling grants 0 experience.
            ---
            
            if (victim:IsTraitor() and attacker:IsTraitor()) then
                return;
            end

            if (not victim:IsTraitor() and not attacker:IsTraitor()) then
                return;
            end

            if (victim:IsDetective() and attacker:IsDetective()) then
                return;
            end

            if (not victim:IsTraitor() and attacker:IsDetective()) then
                return;
            end

            local experience;
            local source;

            local xp_traitor_kill   = Sublime.Settings.Get("ttt", "killing_traitors", "number");
            local xp_innocent_kill  = Sublime.Settings.Get("ttt", "killing_innocent", "number");
            local xp_detective_kill = Sublime.Settings.Get("ttt", "killing_detective", "number");

            if (victim:IsTraitor()) then
                experience = xp_traitor_kill;
                source = "for killing a Traitor"
            elseif(victim:IsDetective()) then
                experience = xp_detective_kill;
                source = "for killing a detective";
            else
                experience = xp_innocent_kill;
                source = "for killing a innocent";
            end

            -- Headshot bonus.
            local lastHit   = victim:LastHitGroup();
            local hModifier = Sublime.Settings.Get("kills", "headshot_modifier", "number");

            if (lastHit == HITGROUP_HEAD) then
                experience = experience * hModifier;
            end

            attacker:SL_AddExperience(experience, source);
        end
    end
end);

hook.Add("TTTEndRound", path, function(result)
    local pCount    = player.GetCount();
    local players   = player.GetAll();

    local traitor_win   = Sublime.Settings.Get("ttt", "traitor_winners", "number");
    local innocent_win  = Sublime.Settings.Get("ttt", "innocent_winners", "number");
    local draw_win      = Sublime.Settings.Get("ttt", "draw", "number");

    for i = 1, pCount do
        local ply = players[i];

        if (IsValid(ply)) then
            if (result == WIN_TRAITOR and ply:IsTraitor()) then
                ply:SL_AddExperience(traitor_win, "for winning the round as a Traitor.");
            elseif(result == WIN_INNOCENT and not ply:IsTraitor()) then
                ply:SL_AddExperience(innocent_win, "for winning the round as a Innocent.");
            else
                ply:SL_AddExperience(draw_win, "for winning the round as a Draw.");
            end
        end
    end
end);