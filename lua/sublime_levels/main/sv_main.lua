--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

resource.AddWorkshop("1780556842");

util.AddNetworkString("Sublime.GetLeaderboardsData");
util.AddNetworkString("Sublime.SendLeaderboardsData");
util.AddNetworkString("Sublime.AdminAdjustData");
util.AddNetworkString("Sublime.ResetDatabase");
util.AddNetworkString("Sublime.SendBattalionData")
util.AddNetworkString("Sublime.SendSpecialistData")

local SQL = Sublime.GetSQL();

net.Receive("Sublime.GetLeaderboardsData", function(_, ply)
    if (ply.SublimeLeaderboardsDataCooldown and ply.SublimeLeaderboardsDataCooldown > CurTime()) then
        return;
    end

    -- Current page.
    local cPage     = net.ReadUInt(32);
    local start    = 14 * (cPage - 1);
    local data      = sql.Query("SELECT SteamID, Level, Experience, TotalExperience, NeededExperience, Name FROM Sublime_Levels ORDER BY TotalExperience DESC");

    local query = mysql:Select("ix_characters")
        query:Select("name")
        query:Select("level")
        query:Select("totalxp")
        query:OrderByDesc("level")
        query:Limit("" .. start .. "," .. start + 14 .. "")
        
        query:Callback(function(result)
            local players = {};
            
            for i = start, start + 14 do
                local pData = result[i];

                if (not pData) then
                    continue;
                end

                table.insert(players, {
                    name        = pData.name,
                    level       = pData.level,
                    xp          = pData.totalxp
                });
            end

            net.Start("Sublime.SendLeaderboardsData");
                net.WriteUInt(#players, 32);
                net.WriteUInt(math.ceil(#result / 14), 32);
                net.WriteTable(players)
            net.Send(ply);

        end)
    query:Execute()
    
    ply.SublimeLeaderboardsDataCooldown = CurTime() + .5;
end);

net.Receive("Sublime.SendBattalionData", function(_, ply)
    if (ply.SublimeBattalionDataCooldown and ply.SublimeBattalionDataCooldown > CurTime()) then
        return;
    end
    -- Current page.
    local cPage     = net.ReadInt(32);
    local index     = net.ReadInt(32);
    local fac       = ix.faction.indices[index]
    local start     = 14 * (cPage - 1);
    if (start <= 0) then start = 1 end
    local query = mysql:Select("ix_characters")
        query:Select("name")
        query:Select("rank")
        query:Select("faction")
        query:Select("last_join_time")
        query:Select("id")
        query:Where("faction", fac.uniqueID)
        query:OrderByDesc("rank")
        query:Limit("" .. start .. "," .. start + 14 .. "")
        
        query:Callback(function(result)
            local players = {};
            for i = start, start + 14 do
                local pData = result[i];

                if (not pData) then
                    continue;
                end

                table.insert(players, {
                    id          = pData.id,
                    name        = pData.name,
                    rank        = pData.rank,
                    last_seen   = pData.last_join_time,
                    faction     = pData.faction
                });
            end

            net.Start("Sublime.SendBattalionData")
                net.WriteInt(math.ceil(#result / 14), 32);
                net.WriteTable(players)
                net.WriteInt(index, 32)
            net.Send(ply)

        end)
    query:Execute()
    
    ply.SublimeBattalionDataCooldown = CurTime() + .5;

end);

net.Receive("Sublime.SendSpecialistData", function(_, ply)
    if (ply.SublimeBattalionDataCooldown and ply.SublimeBattalionDataCooldown > CurTime()) then
        return;
    end
    -- Current page.
    local cPage     = net.ReadInt(32);
    local index     = net.ReadInt(32);
    local fac       = ix.faction.indices[index]
    local start     = 14 * (cPage - 1);
    if (start <= 0) then start = 1 end
    local query = mysql:Select("ix_characters")
        query:Select("name")
        query:Select("specialty")
        query:Select("last_join_time")
        query:Select("id")
        query:Where("faction", fac.uniqueID)
        query:WhereNotEqual("specialty", "none")
        query:OrderByDesc("specialty")
        query:Limit("" .. start .. "," .. start + 14 .. "")
        
        query:Callback(function(result)
            local players = {};
            for i = start, start + 14 do
                local pData = result[i];

                if (not pData) then
                    continue;
                end

                table.insert(players, {
                    id          = pData.id,
                    name        = pData.name,
                    specialty        = pData.specialty,
                    last_seen   = pData.last_join_time,
                    faction     = pData.faction
                });
            end

            net.Start("Sublime.SendSpecialistData")
                net.WriteInt(math.ceil(#result / 14), 32);
                net.WriteTable(players)
                net.WriteInt(index, 32)
            net.Send(ply)

        end)
    query:Execute()
    
    ply.SublimeBattalionDataCooldown = CurTime() + .5;

end);

local GIVE_LEVELS   = 0x1;
local TAKE_LEVELS   = 0x2;
local GIVE_SKILLS   = 0x3;
local TAKE_SKILLS   = 0x4;
local GIVE_XP       = 0x5;
local RESET_XP      = 0x6;

net.Receive("Sublime.AdminAdjustData", function(_, ply)
    if (not Sublime.Config.ConfigAccess[ply:GetUserGroup()]) then
        return;
    end

    local toAdjust  = net.ReadUInt(4);
    local value     = net.ReadUInt(32);
    local target    = net.ReadEntity();
    local steamid   = target:SteamID64();

    if (not IsValid(target)) then
        return;
    end

    if (toAdjust == GIVE_LEVELS) then
        local cLevel    = target:SL_GetLevel();
        local after     = cLevel + value;
        local max       = Sublime.Settings.Get("other", "max_level", "number");

        if (after > max) then
            return;
        end

        if (value < 1) then
            return;
        end

        target:SL_LevelUp(value);

        return;
    end

    if (toAdjust == TAKE_LEVELS) then
        local cLevel    = target:SL_GetLevel();
        local after     = cLevel - value;

        if (after < 0) then
            after = 1
        end

        target:SetNW2Int("sl_level", after);
        target:SetNW2Int("sl_experience", 0);

        target:GetCharacter():SetData("experience", 0)
        target:GetCharacter():SetData("sl_level", after)
        target:GetCharacter():SetData("sl_experience", 0)

        Sublime.Print("%s has taken %i levels from %s.", ply:Nick(), value, target:Nick());

        return;
    end

    if (toAdjust == GIVE_SKILLS) then
        target:SL_AddSkillPoint(value);

        return;
    end

    if (toAdjust == TAKE_SKILLS) then
        local sPoints   = target:SL_GetInteger("ability_points", 0);
        local after     = sPoints - value;

        if (after < 0) then
            after = 0;
        end

        target:SL_SetInteger("ability_points", after);

        target:GetCharacter():SetData("ability_points", after)

        Sublime.Print("%s has now %i skill points to use.", target:Nick(), after);

        return;
    end

    if (toAdjust == GIVE_XP) then
        target:SL_AddExperience(value, "from an Admin.", true, false);
        return;
    end

    if (toAdjust == RESET_XP) then
        target:SetNW2Int("sl_experience", 0);

        target:GetCharacter():SetData("experience", 0)
        target:GetCharacter():SetData("sl_experience", 0)

        target.SL_ExperienceGained    = 0;
        target.SL_ExperienceCount     = 0;

        Sublime.Print("%s has reset %s's experience.", ply:Nick(), target:Nick());

        return;
    end
end);

net.Receive("Sublime.ResetDatabase", function(_, ply)
    if (not ply:IsSuperAdmin()) then
        return;
    end

    sql.Query("DROP TABLE Sublime_Levels");
    sql.Query("DROP TABLE Sublime_Skills");
    sql.Query("DROP TABLE Sublime_Data");

    RunConsoleCommand("changelevel", game.GetMap());
end);