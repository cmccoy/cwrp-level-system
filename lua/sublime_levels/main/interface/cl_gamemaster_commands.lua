--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

local panel = {};

local approach = math.Approach;

function panel:CreateCategory(category, data)
    local nextCategory = #self.Categories + 1;

    self.Categories[nextCategory] = self.ScrollPanel:Add("DPanel");
    local cat = self.Categories[nextCategory];

    cat.Items = {};
    cat.Name = category:gsub("^%l", string.upper);
    cat.Dropped = false;
    cat.Tall = 15;
    cat.FirstTall = cat.Tall;
    cat.Paint = function(panel, w, h)
        draw.RoundedBox(8, 0, 0, w, h, Color(0, 0, 0, 100));
        
        Sublime:DrawTextOutlined(panel.Name, "Sublime.18", 5, 15, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
    end

    cat.PerformLayout = function(panel, w, h)
        for i = 1, #panel.Items do
            local item = panel.Items[i];

            if (IsValid(item)) then
                item:SetPos(5, 30 + 65 * (i - 1));
                item:SetSize(w - 10, 60);
            end
        end

        panel.DropDown:SetPos(w - 30, 3);
        panel.DropDown:SetSize(24, 24);
    end

    cat.AddItem = function(text, lines)
        local nextItem = #cat.Items + 1;

        cat.Items[nextItem] = cat:Add("DPanel");
        local setting = cat.Items[nextItem];

        setting.Paint = function(panel, w, h)
            draw.RoundedBox(8, 0, 0, w, h, self.CA(self.C.Outline, 100));
            if(lines <= 2) then
                Sublime:DrawTextOutlined(text, "Sublime.16", 10, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
            else
                Sublime:DrawTextOutlined(text, "Sublime.16", 10, 30, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
            end
        end

    end

    cat.DropDown = cat:Add("DButton");
    cat.DropDown:SetText("");
    cat.DropDown:SetCursor("arrow");

    cat.DropDown.IconSize = 18;
    cat.DropDown.CurrentRotation = 90;

    cat.DropDown.Paint = function(panel, w, h)
        Sublime:DrawMaterialRotatedOutline(w / 2, h / 2, panel.IconSize, panel.IconSize, Sublime.Materials["SL_LeftArrow"], self.C.Black, self.C.White, panel.CurrentRotation);
        
        if (cat.Dropped) then
            if (panel.CurrentRotation > 90) then
                panel.CurrentRotation = approach(panel.CurrentRotation, 90, 4);
            end
        else
            if (panel.CurrentRotation < 180) then
                panel.CurrentRotation = approach(panel.CurrentRotation, 180, 4);
            end
        end
    end

    cat.DropDown.DoClick = function()
        cat.Dropped = not cat.Dropped;

        self:InvalidateLayout(false);
    end

    cat.AddItem(data.text, data.lines);

    return cat;
end

function panel:CreateCategories(refresh)
    if (refresh) then
        self.ScrollPanel:Clear();
        table.Empty(self.Categories);
    end

    local settings = util.JSONToTable(file.Read("sublime_levels_settings.txt", "DATA"));
    for category, data in pairs(ixGamemasterCommands) do
        self:CreateCategory(category, data)
    end
end

---
--- Init
---
function panel:Init()
    self.L  = Sublime.L;
    self.C  = Sublime.Colors;
    self.CA = ColorAlpha;

    self.Key = 0;
    self.KeyPanel = nil;

    self.Categories = {};

    self.ScrollPanel = self:Add("DScrollPanel");
    local vBar = self.ScrollPanel:GetVBar();

    vBar:SetHideButtons(true);
    
    vBar.Color = Color(0, 0, 0, 50);
    vBar.Paint = function(panel, w, h)
        draw.RoundedBox(8, 2, 0, w - 4, h, panel.Color);    
    end

    vBar.btnGrip.Alpha = 25;
    vBar.btnGrip.Paint = function(panel, w, h)
        draw.RoundedBox(8, 2, 0, w - 4, h, self.C.Outline);

        panel.Alpha = Sublime:DoHoverAnim(panel, panel.Alpha, {75, 2}, {25, 2}, panel:GetParent().Dragging); 
    end

    self:CreateCategories();
end

---
--- OnKeyCodePressed
---
function panel:Think()
    
end

---
--- OnRemove
---
function panel:OnRemove()
    Sublime.Settings.Save();
end

---
--- PerformLayout
---
function panel:PerformLayout(w, h)
    self.ScrollPanel:SetPos(0, 0);
    self.ScrollPanel:SetSize(w, h - 5);

    local yPos      = 5;
    local size      = self.ScrollPanel:GetVBar().Enabled and 20 or 10
    local padding   = 5;

    for i = 1, #self.Categories do
        local item = self.Categories[i];

        if (IsValid(item)) then

            local count     = table.Count(item.Items);
            local height    = item.Dropped and (30 + (65 * count)) or 30;

            item:SetPos(5, yPos);
            item:SetSize((w - padding) - size, height);

            yPos = yPos + (height + padding);
        end
    end
end

---
--- Paint
---
function panel:Paint(w, h)

end
vgui.Register("Sublime.GamemasterCommands", panel, "EditablePanel");