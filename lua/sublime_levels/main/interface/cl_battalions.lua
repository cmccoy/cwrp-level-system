local panel = {};

---
--- CreateButtons
---
function panel:CreateButtons()
    for i = 1, #self.Buttons do
        local data = self.Buttons[i];
        local access = data.access;

        if (not access) then
            continue;
        end

        self.CreatedButtons[i] = self.OptionsHolder:Add("DButton");
        self.CreatedButtons[i]:SetText("");
        self.CreatedButtons[i].Alpha = 100;
        self.CreatedButtons[i].Paint = function(panel, w, h)
            local color1 = self.CA(Sublime:LightenColor(data.color, 50), panel.Alpha);
            local color2 = self.CA(Sublime:DarkenColor(data.color, 25), panel.Alpha);

            Sublime:DrawRoundedGradient(panel, 8, 0, 0, w, h, color1, color2);
            Sublime:DrawTextOutlined(data.name, "Sublime.16", 26, h / 2, self.C.White, self.C.Black, TEXT_ALIGN_LEFT, true);
            
            local icon_size = 16;
            Sublime:DrawMaterialOutline(5, (h / 2) - (icon_size / 2), icon_size, icon_size, data.mat, self.C.Black, self.C.White);
            
            if (self.Selected == panel) then
                Sublime:DrawMaterialRotatedOutline(w - 10, h / 2, icon_size, icon_size, Sublime.Materials["SL_LeftArrow"], self.C.Black, self.C.White, 180);
            end

            panel.Alpha = Sublime:DoHoverAnim(panel, panel.Alpha, {150, 2}, {100, 2});
        end

        self.CreatedButtons[i].DoClick = function(s)
            if (data.clickoverride) then
                data.clickoverride(s);

                return true;
            end
            if (IsValid(self.CreatedPanel)) then
                self.CreatedPanel:Remove();
            end

            self.Selected     = s;
            self.CreatedPanel = self:Add(data.ui);
            self.CreatedPanel:SetPos(150, 0);
            self.CreatedPanel:SetSize(self:GetWide() - 150, self:GetTall());

            if (data.func) then
                data.func(self.CreatedPanel);
            end
            
            net.Start("Sublime.SendBattalionData")
                net.WriteInt(0, 32);
                net.WriteInt(data.index, 32)
            net.SendToServer()

            return true;
        end

        self.CreatedButtons[i].OnCursorEntered = function()
            surface.PlaySound("sublime_levels/button.mp3");
        end

    end
end

---
--- Init
---
function panel:Init()
    self.CreatedButtons = {};
    self.L              = Sublime.L;
    self.WarningMessage = "";
    self.WarningColor   = Sublime.Colors.White;
    self.C              = Sublime.Colors;
    self.CA             = ColorAlpha;
    self.Player         = LocalPlayer(); 

    self.Buttons = {}
    -- Buttons to create
    for k, v in pairs(ix.faction.teams) do
        if (v.isActive) then
            local t = {}
            t.name = v.name
            t.ui = "Sublime.BattalionsSubMenu"
            t.color = v.color
            t.mat = Sublime.Materials["SL_Settings"]
            t.index = v.index
            t.access = true
            table.insert(self.Buttons, t)
        end
    end

    self.OptionsHolder = self:Add("DPanel");
    self.OptionsHolder.PerformLayout = function(s, w, h)
        for i = 1, #self.CreatedButtons do
            local button = self.CreatedButtons[i];

            if (IsValid(button)) then
                button:SetPos(5, 5 + (35 * (i - 1)));
                button:SetSize(w - 11, 30)
            end
        end
    end

    self.OptionsHolder.Paint = function(s, w, h)
        surface.SetDrawColor(0, 0, 0, 100);
        surface.DrawRect(0, 0, w, h);

        surface.SetDrawColor(Sublime.Colors.Outline);
        surface.DrawRect(w - 1, 0, 1, h);
    end

end

---
--- PerformLayout
---
function panel:PerformLayout(w, h)
    self.OptionsHolder:SetPos(0, 0);
    self.OptionsHolder:SetSize(150, h);
end

---
--- Think
---
function panel:Think()
    if (not self.PostInitCalled) then
        self:PostInit();

        self.PostInitCalled = true;
    end
end

---
--- PostInit
---
function panel:PostInit()
    self:CreateButtons();
end

---
--- SetWarningMessage
---
function panel:SetWarningMessage()
    -- deprecated
end

---
--- Paint
---
function panel:Paint(w, h)
end
vgui.Register("Sublime.Battalions", panel, "EditablePanel");