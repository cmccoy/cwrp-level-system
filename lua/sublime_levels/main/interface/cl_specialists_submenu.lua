local path          = Sublime.GetCurrentPath();
local data          = {};
local panel         = {};
local index         = 4;
local panelRefrence = nil;
local maxPages      = 1;
local comma         = string.Comma;

-- Colors
local mainOffsetColor   = Color(34, 44, 44);
local otherOffsetColor  = Color(45, 105, 99);

---
--- AddPlayerToLeaderboard
---
function panel:AddPlayerToBattalion(parent, id, name, specailty, last_seen)
    local nextPlayer = #parent.Players + 1;

    parent.Players[nextPlayer] = parent:Add("DPanel");
    parent.Players[nextPlayer]:SetPos(5, 5 + 35 * (nextPlayer - 1));
    parent.Players[nextPlayer]:SetSize(parent:GetWide() - 10, 30);
    parent.Players[nextPlayer].PerformLayout = function(s, w, h)
        if (IsValid(s.Avatar)) then
            s.Avatar:SetPos(41, (h / 2) - s.Avatar.MaskSize / 2);
            s.Avatar:SetSize(s.Avatar.MaskSize, s.Avatar.MaskSize);
        end
    end
    
    parent.Players[nextPlayer].Paint = function(s, w, h)
        draw.RoundedBox(8, 0, 0, w, h, self.CA(self.C.Outline, 50));

        Sublime:DrawTextOutlined(id, "Sublime.14", 5, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
        Sublime:DrawTextOutlined(name, "Sublime.14", 40, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
        Sublime:DrawTextOutlined(specailty, "Sublime.14", 250, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
        Sublime:DrawTextOutlined(string.NiceTime(os.time() - last_seen) .. " ago", "Sublime.14", w - 100, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);
    end
    
end

---
--- FillLeaderboard
---
function panel:FillLeaderboard()
    for k, v in ipairs(data) do
        self:AddPlayerToBattalion(self.PlayerHolders, v.id, v.name, v.specialty, v.last_seen);
    end

end

---
--- RebuildLeaderboard
---
function panel:RebuildLeaderboard()
    for i = 1, #self.PlayerHolders.Players do
        local panel = self.PlayerHolders.Players[i];

        if (IsValid(panel)) then
            panel:Remove();
        end
    end

    table.Empty(self.PlayerHolders.Players);
    self:FillLeaderboard();
end

---
--- RequestData
---
function panel:RequestData()
    net.Start("Sublime.SendBattalionData")
        net.WriteInt(self.Page, 32);
        net.WriteInt(index, 32)
    net.SendToServer();

    self.Player.Sublime_SpecialistCooldown = CurTime() + .5;
end

---
--- Init
---
function panel:Init()
    self.Page       = 1;
    self.Player     = LocalPlayer();
    self.ShouldFill = false;
    self.L          = Sublime.L;
    self.C          = Sublime.Colors;
    self.CA         = ColorAlpha;

    self.PlayerHolders = self:Add("DPanel");
    self.PlayerHolders.Players = {};
    self.PlayerHolders.Paint = function(panel, w, h)
        draw.RoundedBox(8, 0, 0, w, h, Color(0, 0, 0, 100));
    end

    self.Next = self:Add("DButton");
    self.Next:SetText("");
    self.Next.Alpha = 100;
    self.Next.Paint = function(s, w, h)
        draw.RoundedBox(8, 0, 0, w, h, self.CA(self.C.Outline, s.Alpha));

        surface.SetDrawColor(255, 255, 255);
        surface.SetMaterial(Sublime.Materials["SL_LeftArrow"]);
        surface.DrawTexturedRectRotated(w - 12, h / 2, 12, 12, 180);

        Sublime:DrawTextOutlined(self.L("leaderboards_next"), "Sublime.14", 5, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, true);

        if (s:IsHovered()) then
            s.Alpha = math.Approach(s.Alpha, 200, 4);
        else
            s.Alpha = math.Approach(s.Alpha, 100, 2);
        end
    end

    self.Next.DoClick = function()
        if (self.Page >= maxPages) then
            return;
        else
            self.Page = self.Page + 1;
        end

        self:RequestData();
    end

    self.Next.OnCursorEntered = function()
        surface.PlaySound("sublime_levels/button.mp3");
    end

    self.Previous   = self:Add("DButton");
    self.Previous:SetText("");
    self.Previous.Alpha = 100;
    self.Previous.Paint = function(s, w, h)
        draw.RoundedBox(8, 0, 0, w, h, self.CA(self.C.Outline, s.Alpha));

        surface.SetDrawColor(255, 255, 255);
        surface.SetMaterial(Sublime.Materials["SL_LeftArrow"]);
        surface.DrawTexturedRect(4, (h / 2) - 6, 12, 12);

        Sublime:DrawTextOutlined(self.L("leaderboards_previous"), "Sublime.14", w - 8, h / 2, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER);

        if (s:IsHovered()) then
            s.Alpha = math.Approach(s.Alpha, 200, 4);
        else
            s.Alpha = math.Approach(s.Alpha, 100, 2);
        end
    end

    self.Previous.DoClick = function()
        if (self.Page <= 1 or self.Player.Sublime_SpecialistCooldown > CurTime()) then
            return;
        else
            self.Page = self.Page - 1;
        end

        self:RequestData();
    end

    self.Previous.OnCursorEntered = function()
        surface.PlaySound("sublime_levels/button.mp3");
    end

    panelRefrence = self;
end

---
--- Think
---
function panel:Think()
    if (self.ShouldFill and next(data) and self.PlayerHolders:GetWide() > 64) then
        self:FillLeaderboard();

        self.ShouldFill = false;
    end
end

---
--- PerformLayout
---
function panel:PerformLayout(w, h)
    self.PlayerHolders:SetPos(5, 40);
    self.PlayerHolders:SetSize(w - 10, h - 80);

    self.Next:SetPos(w - 125, h - 35);
    self.Next:SetSize(120, 30);

    self.Previous:SetPos(5, h - 35);
    self.Previous:SetSize(120, 30);
end

---
--- Paint
---
function panel:Paint(w, h)
    surface.SetDrawColor(0, 0, 0, 0);
    surface.DrawRect(0, 0, w, h);

    draw.RoundedBox(8, 5, 5, w - 10, 30, Color(0, 0, 0, 100));

    Sublime:DrawTextOutlined("#", "Sublime.14", 15, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);
    Sublime:DrawTextOutlined("Name", "Sublime.14", 50, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);
    Sublime:DrawTextOutlined("Specialty", "Sublime.14", 250, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);
    Sublime:DrawTextOutlined("Last Seen", "Sublime.14", w - 100, 20, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);

    -- Leaderboard Pages.
    Sublime:DrawTextOutlined(self.Page .. "/" .. maxPages, "Sublime.14", (w / 2) - 7, h - 19, Sublime.Colors.White, Sublime.Colors.Black, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER);
end
vgui.Register("Sublime.SpecialistSubMenu", panel, "EditablePanel");

hook.Add("Sublime.SendSpecialistData", path, function(newData, max, newIndex)

    data = newData;
    maxPages = max;
    index = newIndex
    if (IsValid(panelRefrence)) then
        panelRefrence:RebuildLeaderboard();
    end
end);