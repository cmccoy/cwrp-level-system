--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

local path  = Sublime.GetCurrentPath();
local SKILL = {};

-- This is the name of the skill.
SKILL.Name              = "Armor Regeneration";

-- The description of the skill.
SKILL.Description       = "Regenerates your armor when you're not in combat. Regenerates up to 2% of your armor up to 50 every other second.";

-- If the category of the skill does not exist then we will automatically create it.
SKILL.Category          = "Utility"

-- This is the identifier in the database, needs to be unqiue.
SKILL.Identifier        = "armor_regeneration";

-- The amount of buttons on the skill page.
SKILL.ButtonAmount      = 5;
SKILL.AmountPerPoint    = 0.02;

-- Should we enable this skill?
SKILL.Enabled           = true;

-- Custom variables used by this skill only.

-- How often should the player receive health? These are in seconds.
SKILL.Cooldown = 5;

-- How long does the player need to wait before receiving health after taking damage?
SKILL.Wait = 10;

-- NOTE:
-- If you want to change how much health the player receives then you need to change the SKILL.AmountPerPoint variable.
-- The variable is extremely sensitive towards numbers, the default is 0.002. If you think this is too little then change it to 0.004;
-- The higher the variable number the more health the player will receive.

-- If you change the number then make sure to change the description to match its current number.
if (SERVER) then
    hook.Add("PlayerSpawn", path, function(pl)
        if (pl:Alive() && SKILL.Enabled) then
            local points = pl:SL_GetInteger(SKILL.Identifier, 0) * SKILL.AmountPerPoint;

            if (points > 0) then
                pl.SublimeLevels_CanRegenerate_Armor = CurTime() + SKILL.Wait;
            elseif(points <= 0 && pl.SublimeLevels_CanRegenerate) then
                pl.SublimeLevels_CanRegenerate_Armor = nil
            end
        end
    end);

    -- Just the initial cooldown
    local Cooldown = CurTime() + 2;
    hook.Add("Tick", path, function()
        if (Cooldown > CurTime()) then
            return;
        end

        local players = player.GetAll();
        for i = 1, #players do
            local player = players[i];

            if (player.SublimeLevels_CanRegenerate_Armor and player.SublimeLevels_CanRegenerate_Armor <= CurTime()) then
                local points    = player:SL_GetInteger(SKILL.Identifier, 0) * SKILL.AmountPerPoint;

                if (points > 0) then
                    local armor = player:Armor();

                    if (armor >= 50) then
                        continue;
                    end

                    local newArmor = math.Clamp(armor + (points * 50), 1, 50);
                    player:SetArmor(newArmor);
                end
            end
        end

        Cooldown = CurTime() + SKILL.Cooldown;
    end);
end

Sublime.AddSkill(SKILL);