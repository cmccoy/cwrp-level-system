--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

local path  = Sublime.GetCurrentPath();
local SQL   = Sublime.GetSQL();

util.AddNetworkString("Sublime.UpgradeSkillNotify");
util.AddNetworkString("Sublime.UpgradeSkill");
util.AddNetworkString("Sublime.ResetSkills");

---
--- SL_AddSkillPoint
---
function Sublime.Player:SL_AddSkillPoint(amount)
    local amount = (amount or 1);

    if (not isnumber(tonumber(amount))) then
        Sublime.Print("Argument 'Amount' in function SL_AddSkillPoint needs to be a number.");

        return false;
    end

    local new = self:GetCharacter():GetData("ability_points", 0) + amount;
    self:SL_SetInteger("ability_points", new);

    self:GetCharacter():SetData("ability_points", new)

    Sublime.Print("%s has now %i skill points to use.", self:Nick(), new);
    hook.Run("SL.PlayerReceivedSkillPoint", self, amount, new);
end

net.Receive("Sublime.UpgradeSkill", function(_, ply)
    if (not IsValid(ply)) then
        return;
    end

    local skill     = net.ReadString();
    local steamid   = ply:SteamID64();

    if (ply:SL_GetAbilityPoints() >= 1) then
        local max = Sublime.GetMaxSkill(skill);

        if (not max or ply:SL_GetInteger(skill, 0) >= max) then
            return;
        end

        ply:SL_SetInteger(skill, ply:SL_GetInteger(skill, 0) + 1);
        ply:SL_SetInteger("ability_points", ply:SL_GetAbilityPoints() - 1);

        ply:GetCharacter():SetData("ability_points",  ply:SL_GetAbilityPoints() - 1)

        ---
        --- Call necessary functions
        ---

        local skillTable = Sublime.GetSkill(skill);
        if (skillTable and skillTable.OnBuy) then
            skillTable.OnBuy(ply);
        end

        ---
        --- Update data
        ---

        local data = ply:GetCharacter():GetData("skill_data", {})

        data[skill] = (data[skill] and data[skill] + 1) or 1;

        ply:GetCharacter():SetData("skill_data", data)
    
        net.Start("Sublime.UpgradeSkillNotify")
            net.WriteString(skillTable.Name);
            net.WriteUInt(ply:SL_GetInteger(skill, 0), 16);
            net.WriteUInt(skillTable.ButtonAmount, 16);
        net.Send(ply);
    end
end);

net.Receive("Sublime.ResetSkills", function(_, ply)
    if (not IsValid(ply) or ply:SL_GetAbilityPoints() == (ply:SL_GetLevel() - 1)) then
        return;
    end

    local steamid = ply:SteamID64();

    ---
    --- Reset the skill data.
    ---
    local data = ply:GetCharacter():GetData("skill_data", {})

    for k, v in pairs(data) do
        data[k] = 0;
        ply:SL_SetInteger(k, 0);

        local skillTable = Sublime.GetSkill(k);
        if (skillTable and skillTable.OnBuy) then
            skillTable.OnBuy(ply);
        end
    end

    ply:GetCharacter():SetData("skill_data", data)

    ---
    --- Give back the points
    ---
    local plyShouldReceive = ply:SL_GetLevel() - 1;
    ply:SL_SetInteger("ability_points", plyShouldReceive);

    ply:GetCharacter():SetData("ability_points", plyShouldReceive)
end);

hook.Add("Sublime.InitializeSkills", path, function(ply, data)
    if (not IsValid(ply)) then
        return;
    end

    -- Set every integer in the skills table to 0 first
    for k, v in ipairs(Sublime.Skills) do
        ply:SL_SetInteger(v.Identifier, 0);
    end

    -- then set the correct values
    -- this is to ensure that if you create more skills than the default ones
    -- then it will save properly.
    for k, v in pairs(data) do
        ply:SL_SetInteger(k, v);
    end
end);