--[[------------------------------------------------------------------------------
 *  Copyright (C) Fluffy(76561197976769128 - STEAM_0:0:8251700) - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
--]]------------------------------------------------------------------------------

Sublime.Config = Sublime.Config or {};

---
--- 
--- EXPERIENCE
---
--- You can mess around with these settings but if the numbers are either to low or too high then
--- you might create imbalance in gaining levels/experience.

--- The base experience the player needs to get to level 2.
--- This is used for a numerous amount of things,
--- such as, calculating how much experience they need to gain a level, etc.
--- Changing this after players have received levels won't change the players levels.
--- But it will affect their next levels, and newcomers will be affected, too.
Sublime.Config.BaseExperience = 500;

---
--- This is how much we should times the XP by
---
--- This is how much XP the players would have to get in order to reach level 11.
--- The equation would be:
--- (player_level * BaseExperience) * ExperienceTimes;
--- (10 * 1000) * 1.57 = 15,700;
---
--- Changing the ExperienceTimes variable below will significantly change the difficulty of gaining levels.
--- Say if you changed it to 0.5;
--- It would be:
--- (10 * 1000) * 0.5 = 5,000;
---
--- However, if you doubled it(3)
--- It would be:
--- (10 * 1000) * 3 = 30,000;
--- 
Sublime.Config.ExperienceTimes = math.pi / 2;

---
--- This is for Tommy's SpecDM
--- Should we allow players to gain experience even though they're in SpecDM?
---
Sublime.Config.SpecExperience = false;

---
--- 
--- ACCESS
---

--- Who has access to the config in-game?
Sublime.Config.ConfigAccess = {
    ["owner"] = true,
    ["superadmin"] = true
};

Sublime.Config.VipBonus = {
    ["superadmin"] = true,
    ["admin"] = true,
    ["vip"] = true,
}

ixPlayerCommands = {}
ixOfficerCommands = {}
ixGamemasterCommands = {}
ixStaffCommands = {}

function ixAddToCommandTable(tbl, lines, command, text) 
    tbl[command] = {text = text, lines = lines}
end

ixAddToCommandTable(ixPlayerCommands, 2, "B Menu", [[Requires: None
Press B to see a quick command menu]])

ixAddToCommandTable(ixPlayerCommands, 2, "/aos", [[Requires: Character Name && Must be CG Battalion
Sets the specified character to AoS status]])

ixAddToCommandTable(ixPlayerCommands, 2, "/unaos", [[Requires: Character Name && Must be CG Battalion
Removes the specified character's AoS status]])

ixAddToCommandTable(ixPlayerCommands, 2, "/citations", [[Requires: Character Name && Must be CG Battalion
Views all previous arrests on the specified character]])

ixAddToCommandTable(ixPlayerCommands, 2, "/join", [[Requires: Battalion Name
Joins the specified battalion]])
                        
ixAddToCommandTable(ixPlayerCommands, 2, "/leave", [[Requires: Battalion Name
Joins the specified battalion]])

ixAddToCommandTable(ixPlayerCommands, 2, "/comms", [[Requires: Message
Sends a message over the comms chat channel]])

ixAddToCommandTable(ixPlayerCommands, 2, "/battalionChat", [[Requires: Message
Sends a message over your battalion's chat channel]])

ixAddToCommandTable(ixPlayerCommands, 2, "/pm", [[Requires: Message
Directly messages a player]])

ixAddToCommandTable(ixPlayerCommands, 2, "/helmet", [[Requires: None
Takes your helmet off/on]])

ixAddToCommandTable(ixOfficerCommands, 3, "/claim", [[Requires: Room Name | Reason
Default values: sim | citadel
Allows you to edit the room board and spawn entities/props in the room]])

ixAddToCommandTable(ixOfficerCommands, 2, "/fly", [[Requires: None
Allows you to fly around in your claimed room]])

ixAddToCommandTable(ixOfficerCommands, 2, "/broadcast", [[Requires: Message
Sends a message over the base's PA system]])

ixAddToCommandTable(ixOfficerCommands, 2, "/unclaim", [[Requires: None
Unclaims your currently claimed room]])

ixAddToCommandTable(ixOfficerCommands, 2, "/invite", [[Requires: Character name
Invites the specified character to your battalion]])

ixAddToCommandTable(ixOfficerCommands, 2, "/kick", [[Requires: Character name
Kicks the specified character from your battalion]])

ixAddToCommandTable(ixOfficerCommands, 2, "/kickByID", [[Requires: Clone ID
Kicks the specified clone ID from your battalion]])

ixAddToCommandTable(ixOfficerCommands, 2, "/promote", [[Requires: Character name
Promotes the specified clone]])

ixAddToCommandTable(ixOfficerCommands, 2, "/demote", [[Requires: Character name
Demotes the specified clone to their previous rank]])

ixAddToCommandTable(ixOfficerCommands, 2, "/xo", [[Requires: Character name
Promotes the specified clone to Executive Officer]])

ixAddToCommandTable(ixOfficerCommands, 2, "/writemedicreferral", [[Requires: Character name && Must be commander
Writes a referral approving the trooper to attempt medical training]])

ixAddToCommandTable(ixStaffCommands, 2, "/sit", [[Requires: None
Sends you to the sit room]])

ixAddToCommandTable(ixStaffCommands, 2, "/leaveSit", [[Requires: None
Leaves the sit room]])

ixAddToCommandTable(ixStaffCommands, 2, "/unjail", [[Requires: Character Name
Unjails the specified character]])

ixAddToCommandTable(ixStaffCommands, 3, "/unclaimByRoom", [[Requires: Room Name
Default values: sim | citadel
Forcefully unclaims a claimed room]])

ixAddToCommandTable(ixStaffCommands, 2, "/commander", [[Requires: Character name | Battalion name
Promotes the specified clone to Commander of the specified battalion]])

ixAddToCommandTable(ixStaffCommands, 2, "/promoteCommanderByID", [[Requires: STEAM64ID | Battalion
Promotes the specified player to commander of specified battalion]])

ixAddToCommandTable(ixStaffCommands, 2, "/clearCommander", [[Requires: Battalion name
Demotes all commanders from specified battalion]])

ixAddToCommandTable(ixStaffCommands, 2, "!blogs", [[Requires: None
Opens up the logging interface]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterMenu", [[Requires: None
Returns a list of all gamemasters and information]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/defcon", [[Requires: Number
Sets the current defcon to the specified number]])

ixAddToCommandTable(ixGamemasterCommands, 3, "/gamemasterEventChat", [[Requires: Random Name | What to Say
Prints the arguments to chat
Ex: /gamemastereventchat B2-Super Take the base!]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterBroadcast", [[Requires: None
Allows the entire server to hear you]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/startEvent", [[Requires: None
Opens a panel to begin an event]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/endEvent", [[Requires: None
Ends the currently running event]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterMode", [[Requires: None
Puts your character into gamemastermode
Allows you to access physgun | toolgun | spawn entities | enabled no targ]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterBring", [[Requires: Character name
Brings the specified player to you]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterBring", [[Requires: Character name
Goes the specified player to you]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterGodMode", [[Requires: None
Enables godmode on you]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterCloak", [[Requires: None
Turns your character invisible]])

ixAddToCommandTable(ixGamemasterCommands, 3, "/gamemasterSetModel", [[Requires: Model-Name | OPTIONAL: Character name
No target: Sets your character model to the specified model
With target: Sets target's character to model specified]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterSetOwnName", [[Requires: Name
Sets your characters name to specified text.
Should automatically fix if you disconnect]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterSetPlayerName", [[Requires: Target | Name
Sets target's name to specified text.
Should automatically fix if they disconnect]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterSetHealth", [[Requires: Number
Sets your health to the value specified]])

ixAddToCommandTable(ixGamemasterCommands, 2, "/gamemasterNoclip", [[Requires: None
Enables noclip mode for you]])

ixAddToCommandTable(ixGamemasterCommands, 3, "/gamemasterScale", [[Requires: Number | OPTIONAL : Character name
No target: Scales your character model to value, default scale is 1
With target: Scales your target's model to value, default scale is 1]])
